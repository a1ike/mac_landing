"use strict";

$(document).ready(function () {
  $('a[href^="#"]').bind('click.smoothscroll', function (e) {
    e.preventDefault();
    var target = this.hash,
        $target = $(target);
    $('html, body').stop().animate({
      'scrollTop': $target.offset().top - 40
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  });
  $('.phone').inputmask('+7(999)-999-99-99');
  $('.m-about-card__plus').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('m-about-card__plus_active');
    $(this).parent().toggleClass('m-about-card__content_active');
    $(this).prev().toggleClass('m-about-card__text_active');
  });
  new Swiper('.m-doctors__cards', {
    slidesPerView: 'auto',
    spaceBetween: 100,
    centeredSlides: false,
    loop: false,
    scrollbar: {
      el: '.m-doctors__cards .swiper-scrollbar',
      draggable: true,
      hide: false
    },
    breakpoints: {
      1200: {
        spaceBetween: 30,
        centeredSlides: false
      },
      400: {
        slidesPerView: 'auto',
        spaceBetween: 200,
        centeredSlides: true
      }
    }
  });
  $(window).on('load resize', function () {
    if ($(window).width() > 1200) {
      new WOW().init();
    } else {
      $('.m-modal__form-button').detach().appendTo('.m-modal_help');
    }
  });
  $('.m-header__toggle').on('click', function (e) {
    e.preventDefault();
    $('.m-header__nav').slideToggle('fast');
    $('.m-header__info').slideToggle('fast');
    $('.m-header__call').slideToggle('fast');
  });
  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.m-modal').toggle();
  });
  $('.m-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.m-modal').toggle();
  });
});
//# sourceMappingURL=main.js.map
